# cyber-schools-minecraft

This repo contains a complete bukkit minecraft server, with RaspberryJuice installed

## Installation and running
Pulling down this repo should speed up the installation process. If the server stored here doesn't work, then grab and install bukkit from https://getbukkit.org/ and the RaspberryJuice plugin from https://dev.bukkit.org/projects/raspberryjuice.


### MacOS

1. Install Minecraft from the official website: https://minecraft.net/en-us/download/

2. Install Python 3 (if not already installed) and the required library:

    ```pip3 install mcpi```
3. A bash script has already been written to launch the server at a click (start_server.command), run it to launch the server.

4. Launch minecraft, login with an account and then click on Multiplayer. Add a new server at `localhost:25565`, and then join it.

That's it! Running the python scripts (currently inside plugins -> RaspberryJuice, but they could be moved somewhere more standard) will execute them on the minecraft server.

## Helpful Bukkit Minecraft server commands
We'll lock it down in the future so you shouldn't need to use these commands, but for now once you've logged in, in the server console type `op username` to make `username` an admin.

These commands can then be used from in game (remove the forward slash to use them directly from the server terminal)

`/time set day` : sets the time to 10:00am in game

`/gamerule doDaylightCycle false`: halts the advance of time/light

`/weather clear`: stops the rain (this can definitely be turned off in the config somewhere)

## Helpful References

http://www.stuffaboutcode.com/p/minecraft.html